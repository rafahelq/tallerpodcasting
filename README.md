# Taller de Podcasting Libre  
Este repositorio servirá para almacenar todos los documentos para el Taller de Podcasting Libre.  

Hasta la fecha he realizado varios:
+ Linux Center, valencia, el 7 de diciembre de 2018
+ TLP 2019, Santa Cruz de Tenerife, el 18 de julio de 2019
+ HacktoberDay 2020, el 24 de octubre de 2020

![](https://gitlab.com/podcastlinux/tallerpodcasting/raw/master/charla/imagen/tallerpodcasting3.png)   

![](https://gitlab.com/podcastlinux/tallerpodcasting/raw/master/charla/imagen/tallerpodcasting2.png)    

![](https://gitlab.com/podcastlinux/tallerpodcasting/raw/master/charla/imagen/tallerpodcasting.png)    

## Licencia  
Documentación elaborada por Juan Febles y liberada bajo licencia Creative Commons Atribución – Compartir Igual  
<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/">
<img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" />
</a>